This repository only has the Simplified-Chinese version. For the English version, please visit [GitHub](https://github.com/ltndkl/Counter-Strike-Avanzate-Edition).

# _Counter-Strike Avanzate Edition_ 官方更新仓库
[官方贴吧](https://tieba.baidu.com/csae)

[官方电报群](https://t.me/joinchat/JNYAMw3FyuWw81pHaG07JA)

请前往[发行版](https://gitee.com/ltndkl/Counter-Strike-Avanzate-Edition/releases)下载 _CSAE_ 。

请前往[Issues](https://gitee.com/ltndkl/Counter-Strike-Avanzate-Edition/issues)报告BUG或请求新功能。
